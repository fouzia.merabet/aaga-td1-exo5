# coding=utf-8

def algo_k(x0):
    x = x0
    y = x / 10 ** 9
    for i in range(0, y + 1 ):
        z = (x/10**8) % 10
        notSkip = False;
        if ((notSkip) or (3 == z +3)):
            notSkip = True
            if( x < 5 * 10**9):
                x = x + 5 * 10**9
        if ((notSkip) or (4 == z +3)):
            notSkip = True
            x = (x*x / 10**5) % 10**10
        if ((notSkip) or (5 == z +3)):
            notSkip = True
            x = (1001001001 * x) % 10**10
        if ((notSkip) or (6 == z +3)):
            notSkip = True
            if x < 10**8 :
                x = x + 9814055677
            else :
                x = 10**10 - x
        if ((notSkip) or (7 == z +3)):
            notSkip = True
            x= (10**5) *(x % 10**5) + x/10**5
        if ((notSkip) or (8 == z +3)):
            notSkip = True
            x = (1001001001 * x) % 10**10
        if ((notSkip) or (9 == z +3)):
            notSkip = True
            nombre = str(x)
            nombre_1 =""
            for i in range(0 , len(nombre)):
                if int(nombre[i]) > 0 :
                    nombre_1 = nombre_1+str(int(nombre[i]) -1)
                else :
                    nombre_1 = nombre_1+ nombre[i] 
            x = int(nombre_1)
        if ((notSkip) or (10 == z +3)):
            notSkip = True
            if x < 10**5 :
                x= x*x+99999
            else: 
                x = x - 99999
        if ((notSkip) or (11 == z +3)):
            notSkip = True
            while x < 10**9 :
                x = 10*x
        if ((notSkip) or (12 == z +3)):
            notSkip = True
            x = (x * (x - 1)/10**5) % 10**10
        if y > 0 :
            y = y - 1
        else :
            return x
            
def cal_Xn(n, f, X0):
    Xn = X0
    for i in range(n):
        Xn = f(Xn)
    return Xn

def calcule_mu_lam(f , X0):
    #etape 1 : calcul de n
    n = 0
    eq = False 
    Xn = f(X0)
    X2n = f(Xn)
    if Xn == X2n : 
        eq = True
    Xn_1 = Xn
    X2n_1 = X2n
    while eq == False :
        Xn = f(Xn_1)
        X2n = f(f(X2n_1))
        if Xn == X2n : 
            eq = True
        Xn_1 = Xn
        X2n_1 = X2n
        n += 1
   
    #etape 2: calcul de la période représenté par la variable lam
    eq = False
    lam = 0
    Xnp = Xn
    while eq == False: 
        Xn1 = f(Xnp)
        if Xn1 == Xn :
            eq = True
        lam += 1
        Xnp = Xn1

    #etape 3: calcul de mu
    eq = True
    mu = n
    while eq == True:
        Xmu = cal_Xn(mu, f, X0)
        Xmu_p = cal_Xn(mu+lam, f, X0)
        print("mu = "+str(mu)+" Xmu = "+str(Xmu)+" Xmu_p = "+str(Xmu_p))
        if Xmu != Xmu_p :
            eq = False
        mu-=1
    
    return mu , lam

mu , lam = calcule_mu_lam(algo_k, 231654)
print("mu = "+str(mu) +"lamda = "+str(lam))
